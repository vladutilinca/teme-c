﻿using System;
using System.IO;
using System.Linq;

namespace SortedVec
{
    class Program
    {
        static void combineVectors(int[] v1,int[] v2,int[] res,int n, int m)
        {
            int i = 0, j = 0, k = 0;
            while (i < n && j < m)
            {
                if(v1[i]<v2[j])
                {
                    res[k] = v1[i];
                    k++;
                    i++;
                }
                else
                {
                    res[k] = v2[j];
                    j++;
                    k++;
                }
            }

            while (i < n)
            {
                res[k] = v1[i];
                k++;
                i++;
            }
            while (j < m)
            {
                res[k] = v2[j];
                k++;
                j++;
            }
        }
        static void Main(string[] args)
        {
            int i = 0, j = 0;
            string v1 = File.ReadLines("../vectors.txt").Skip(0).Take(1).First();
            string v2 = File.ReadLines("../vectors.txt").Skip(1).Take(1).First();
            Console.WriteLine(v1);
            Console.WriteLine(v2);
            string[] vec1 = v1.Split(',');
            string[] vec2 = v2.Split(',');
            int[] firstVec = new int[vec1.Length];
            int[] secondVec = new int[vec2.Length];
            for (i = 0; i < vec1.Length; i++)
            {
                firstVec[i] = Int32.Parse(vec1[i]);
               // Console.WriteLine(firstVec[i]);
            }
            for (i = 0; i < vec2.Length; i++)
            {
                secondVec[i] = Int32.Parse(vec2[i]);
                //Console.WriteLine(secondVec[i]);
            }
            int[] result = new int[vec1.Length + vec2.Length];
            combineVectors(firstVec, secondVec, result, vec1.Length, vec2.Length);
            for(i=0;i<vec1.Length+vec2.Length;i++)
            {
                Console.WriteLine(result[i]);
            }



        }
    }
}
