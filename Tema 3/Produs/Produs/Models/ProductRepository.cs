﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Produs.Models
{
    public class ProductRepository
    {
        readonly List<Product> _products = new List<Product>
        {
            new Product
            {
                Id=1,
                Name="Mustar",
                Descriprion="iute"
            },
            new Product
            {
                Id=2,
                Name="Malai",
                Descriprion="galben"
            }
        };
        public List<Product> Products => _products;
        public Product GetById(int id)
        {
            return _products.SingleOrDefault(x => x.Id == id);
        }
    }
}
