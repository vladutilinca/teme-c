﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Produs.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }
    }
}
