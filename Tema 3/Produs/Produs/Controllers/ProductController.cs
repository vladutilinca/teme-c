﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Produs.Models;

namespace Produs.Controllers
{
    public class ProductController : Controller
    {
        readonly ProductRepository _repo;
        public ProductController(ProductRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            var prod = _repo.Products;
            return View(prod);

        }
        public IActionResult Index1(string id)
        {
            var prod = _repo.GetById(Int32.Parse(id));
            return View(prod);

        }
    }
}