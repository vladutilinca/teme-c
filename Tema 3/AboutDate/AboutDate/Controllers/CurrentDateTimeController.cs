﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AboutDate.Models;
using Microsoft.AspNetCore.Mvc;

namespace AboutDate.Controllers
{
    public class CurrentDateTimeController : Controller
    {
        readonly CurrentDateTimeRepository _repo;

        public CurrentDateTimeController(CurrentDateTimeRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            var currentDate = _repo.CurrentDate;
            return View(currentDate);
        }
    }
}